package hk.guosen.weix.example;

import hk.guosen.weix.svc.AppService;
import hk.guosen.weix.svc.CORAppService;
import hk.guosen.weix.svc.msg.user.BaseMsg;
import hk.guosen.weix.svc.msg.user.ReqMsgText;
import hk.guosen.weix.svc.msg.user.RespMsg;
import hk.guosen.weix.svc.msg.user.RespMsgText;
import hk.guosen.weix.svlt.WeixServlet;

/**
 * 实现功能：
 * 用户输入1，显示hello
 * 用户输入2,返回goodbye
 * 
 * 此例子使用CORAppService,比较适合模块划分。
 * 可根据模块编写各自的AppService,然后统一通过CORAppService组装。
 * CORAppService将请求依次交给各子服务模块处理。
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 *
 */
public class CORSvlt extends WeixServlet
{
	private static final long serialVersionUID = 1L;

	@Override
	public AppService createAppService()
	{
		CORAppService result = new CORAppService();
		
		result.addSvc(new AppService()
		{
			
			@Override
			public RespMsg onMsg(BaseMsg msg) throws Exception
			{
				if(msg.isTextMsg())
				{
					ReqMsgText req = (ReqMsgText) msg;
					if("1".equals(req.getContent()))
					{//处理1相关的业务逻辑
						RespMsgText resp = new RespMsgText(req, "hello");
						return resp;
					}
				}
				
				return null;
			}
		});
		
		result.addSvc(new AppService(){

			@Override
			public RespMsg onMsg(BaseMsg msg) throws Exception
			{
				if(msg.isTextMsg())
				{
					ReqMsgText req = (ReqMsgText) msg;
					if("2".equals(req.getContent()))
					{//处理2相关的业务逻辑
						RespMsgText resp = new RespMsgText(req, "goodbye");
						return resp;
					}
				}
				
				return null;
			}});
		
		return result;
	}

	@Override
	public String getAppToken()
	{
		//请修改此处返回自己的AppToken。
		return "xxxxxx";
	}

}
