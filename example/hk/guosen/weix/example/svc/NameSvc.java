package hk.guosen.weix.example.svc;

import hk.guosen.weix.client.WeixClient;
import hk.guosen.weix.client.msg.WeixClientId;
import hk.guosen.weix.client.msg.usermng.UserInfoResp;
import hk.guosen.weix.svc.AppService;
import hk.guosen.weix.svc.msg.user.BaseMsg;
import hk.guosen.weix.svc.msg.user.ReqMsgText;
import hk.guosen.weix.svc.msg.user.RespMsg;
import hk.guosen.weix.svc.msg.user.RespMsgText;

/**
 * 此例子包括微信客户端请求的使用，当用户输入任何信息时，向微信请求用户名并返回。
 * 
 * @author cailx
 *
 */
public class NameSvc implements AppService
{
	private WeixClient clt;
	
	public NameSvc()
	{
		//初始化客户端，在此填写向微信申请的appID及Secret
		clt = new WeixClient(new WeixClientId("yyyyy", "zzzzz"));
		//向微信登录，获得accessToke，此后可以向微信请求各种服务。
		clt.startAutoLogin();
	}

	@Override
	public RespMsg onMsg(BaseMsg msg) throws Exception
	{
		if(msg.isTextMsg())
		{
			ReqMsgText req = (ReqMsgText) msg;
			String openid = msg.getFromUserName();
			UserInfoResp userInfo = clt.getUserInfo(openid);
			
			return new RespMsgText(req, userInfo.getNickname());
		}
		
		return null;

	}

}
