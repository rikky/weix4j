package hk.guosen.weix.example.svc;

import hk.guosen.weix.svc.AppService;
import hk.guosen.weix.svc.msg.user.BaseMsg;
import hk.guosen.weix.svc.msg.user.ReqMsgText;
import hk.guosen.weix.svc.msg.user.RespMsg;
import hk.guosen.weix.svc.msg.user.RespMsgText;

/**
 * 应用服务示例， 本例子实现一个ECHO服务，将请求的文本消息原样返回。
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 *
 */
public class EchoService implements AppService
{

	@Override
	public RespMsg onMsg(BaseMsg msg) throws Exception
	{
		if(msg.isTextMsg())
		{//如果请求的是文本消息则处理
			ReqMsgText req = (ReqMsgText) msg;
			//使用请求文本构建一个应答消息
			RespMsgText result = new RespMsgText(req, req.getContent());
			return result;
		}
		else
		{//非文本休息，不处理
			return null;
		}
	}

}
