package hk.guosen.weix.example;

import hk.guosen.weix.example.svc.EchoService;
import hk.guosen.weix.svc.AppService;
import hk.guosen.weix.svlt.WeixServlet;

/**
 * 此例子实现一个简单的ECHO功能，将请求的文本消息原样返回。
 * 
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 *
 */
@SuppressWarnings("serial")
public class EchoSvlt extends WeixServlet
{
	@Override
	public AppService createAppService()
	{
		return new EchoService();
	}

	@Override
	public String getAppToken()
	{
		//请修改此处返回自己的AppToken。
		return "xxxxxx";
	}

}
