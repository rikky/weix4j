package hk.guosen.weix.example;

import hk.guosen.weix.client.WeixClient;
import hk.guosen.weix.client.msg.WeixClientId;
import hk.guosen.weix.client.msg.menu.Button;
import hk.guosen.weix.client.msg.menu.Menu;

/**
 * 微信客户端使用例子。
 * 演示如何创建菜单。菜单分两级，一级菜单包括服务，帮助。
 * 服务菜单包括下单，查询。为点击模式的菜单。
 * 帮助菜单包括公司介绍，联系我们，为连接模式的菜单。
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 *
 */
public class MenuExample
{
	public static void main(String[] args)
	{
		//初始化客户端，在此填写向微信申请的appID及Secret
		WeixClient clt = new WeixClient(new WeixClientId("yyyyy", "zzzzz"));
		//向微信登录，获得accessToke，此后可以向微信请求各种服务。
		clt.startAutoLogin();
		
		Button[] mainButtons = new Button[2];
		mainButtons[0] = Button.newCompositorButton("服务", new Button[]{Button.newClickButton("下单", "order"), Button.newClickButton("查询", "query")});
		mainButtons[1] = Button.newCompositorButton("帮助", new Button[]{Button.newViewButton("公司介绍", "http://www.baidu.com"), Button.newClickButton("联系我们", "http://www.baidu.com")});
		
		clt.createMenu(new Menu(mainButtons));
		
		//不再使用的时候，停止后台登陆线程。
		clt.stopAutoLogin();
	}

}
