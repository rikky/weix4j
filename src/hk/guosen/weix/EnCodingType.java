package hk.guosen.weix;

/**
 * 常用编码格式定义。
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 *
 */
public class EnCodingType
{
	public static final String UTF_8 = "UTF-8";
	public static final String UNICODE = "UNICODE";
	public static final String GBK = "GBK";
	public static final String GB2312 = "GB2312";
	public static final String GB18030 = "GB18030";
	public static final String ISO8859_1 = "iso-8859-1";
}
