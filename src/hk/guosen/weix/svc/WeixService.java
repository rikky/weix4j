package hk.guosen.weix.svc;

/**
 * 微信服务接口。
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 *
 */
public interface WeixService
{
	/**
	 * 微信服务端有消息过来时，此方法被调用。
	 * 
	 * @param xml 请求消息
	 * @return 应答消息
	 * @throws Exception
	 */
	String onMsg(String xml);
}
