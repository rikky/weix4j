package hk.guosen.weix.svc.msg.user;

import hk.guosen.weix.shared.msg.Media;
import hk.guosen.weix.svc.msg.MsgType.RespType;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 回复语音消息
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 *
 */
@XStreamAlias("xml")
public class RespMsgVoice extends RespMsg
{
	@XStreamAlias("Voice")
	private Media voice;
	
	public RespMsgVoice(BaseMsg req, Media voice)
	{
		super(req, RespType.voice.name());
		this.voice = voice;
	}

	public Media getVoice()
	{
		return voice;
	}

	public void setVoice(Media voice)
	{
		this.voice = voice;
	}

}
