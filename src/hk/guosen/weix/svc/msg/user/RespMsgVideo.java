package hk.guosen.weix.svc.msg.user;

import hk.guosen.weix.shared.msg.MediaEx;
import hk.guosen.weix.svc.msg.MsgType.RespType;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 回复视频消息
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 *
 */
@XStreamAlias("xml")
public class RespMsgVideo extends RespMsg
{
	@XStreamAlias("Video")
	private MediaEx video;
	
	public RespMsgVideo(BaseMsg req, MediaEx video)
	{
		super(req, RespType.video.name());
		this.video = video;
	}

	public MediaEx getVideo()
	{
		return video;
	}

	public void setVideo(MediaEx video)
	{
		this.video = video;
	}

}
