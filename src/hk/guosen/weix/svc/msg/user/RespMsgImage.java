package hk.guosen.weix.svc.msg.user;

import hk.guosen.weix.shared.msg.Media;
import hk.guosen.weix.svc.msg.MsgType.RespType;

import com.thoughtworks.xstream.annotations.XStreamAlias;

/**
 * 回复图片消息 

 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 *
 */
@XStreamAlias("xml")
public class RespMsgImage extends RespMsg
{
	@XStreamAlias("Image")
	private Media image;
	
	public RespMsgImage(BaseMsg req, Media image)
	{
		super(req, RespType.image.name());
		this.image = image;
	}

	public Media getImage()
	{
		return image;
	}

	public void setImage(Media image)
	{
		this.image = image;
	}

}
