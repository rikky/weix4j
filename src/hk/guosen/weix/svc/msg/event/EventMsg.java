package hk.guosen.weix.svc.msg.event;

import com.thoughtworks.xstream.annotations.XStreamAlias;

import hk.guosen.weix.svc.msg.MsgType.EventType;
import hk.guosen.weix.svc.msg.user.BaseMsg;

/**
 * 微信事件消息基类
 * 
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 *
 */
public abstract class EventMsg extends BaseMsg
{
	// 事件类型 
	@XStreamAlias("Event")
	private String event;
	
	public String getEvent()
	{
		return event;
	}
	
	public void setEvent(String event)
	{
		this.event = event;
	}

	public boolean isClickEvent()
	{
		return EventType.CLICK.name().equalsIgnoreCase(event);
	}
	
	public boolean isSubscribeEvent()
	{
		return EventType.subscribe.name().equalsIgnoreCase(event);
	}
	public boolean isUnsubscribeEvent()
	{
		return EventType.unsubscribe.name().equalsIgnoreCase(event);
	}
	public boolean isScanEvent()
	{
		return EventType.scan.name().equalsIgnoreCase(event);
	}
	public boolean isLocationEvent()
	{
		return EventType.LOCATION.name().equalsIgnoreCase(event);
	}
}
