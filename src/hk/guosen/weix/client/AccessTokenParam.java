package hk.guosen.weix.client;

/**
 * 用于在请求参数中加上access_token
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 *
 */
public class AccessTokenParam
{
	private String access_token;
	
	public AccessTokenParam()
	{
	}
	
	public AccessTokenParam(String access_token)
	{
		this.access_token = access_token;
	}

	public String getAccess_token()
	{
		return access_token;
	}

	public void setAccess_token(String access_token)
	{
		this.access_token = access_token;
	}
}
