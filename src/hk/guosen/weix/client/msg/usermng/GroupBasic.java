package hk.guosen.weix.client.msg.usermng;

/**
 * 分组基本信息，用于创建分组应答
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 *
 */
public class GroupBasic extends GroupMin
{
	// 分组id，由微信分配 
	public long id;

	public GroupBasic()
	{
		
	}
	
	public GroupBasic(long id, String name)
	{
		super(name);
		this.id = id;
	}
}
