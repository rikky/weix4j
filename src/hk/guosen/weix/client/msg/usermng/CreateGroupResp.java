package hk.guosen.weix.client.msg.usermng;

import hk.guosen.weix.client.msg.BaseResp;

/**
 * 创建用户组应答
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 *
 */
public class CreateGroupResp extends BaseResp
{	
	public GroupBasic group;

}
