package hk.guosen.weix.client.msg.usermng;

/**
 * 修改分组名称请求
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 *
 */
public class UpdateGroupReq
{
	public GroupBasic group;

	public UpdateGroupReq(GroupBasic group)
	{
		super();
		this.group = group;
	}

}
