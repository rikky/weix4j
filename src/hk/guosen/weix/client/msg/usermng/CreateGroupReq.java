package hk.guosen.weix.client.msg.usermng;

/**
 * 创建用户组请求
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 *
 */
public class CreateGroupReq
{	
	public GroupMin group;

	public CreateGroupReq(GroupMin group)
	{
		super();
		this.group = group;
	}
}
