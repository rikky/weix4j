package hk.guosen.weix.client.msg.usermng;

/**
 * 用户组详细信息。
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 *
 */
public class Group extends GroupBasic
{
	//分组内用户数量 
	public int count;

}
