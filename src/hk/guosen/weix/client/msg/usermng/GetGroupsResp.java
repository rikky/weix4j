package hk.guosen.weix.client.msg.usermng;

import hk.guosen.weix.client.msg.BaseResp;

/**
 * 查询分组应答
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 *
 */
public class GetGroupsResp extends BaseResp
{
	public Group[] groups;
}
