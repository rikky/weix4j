package hk.guosen.weix.client.msg.usersvc;

import hk.guosen.weix.shared.msg.Media;

/**
 * 发送给客户的图片消息
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 *
 */
public class UserMsgImage extends UserMsg
{	
	public Media image;
	
	public UserMsgImage(String touser, String media_id)
	{
		super(touser, "image");
		this.image = new Media(media_id);
	}

}
