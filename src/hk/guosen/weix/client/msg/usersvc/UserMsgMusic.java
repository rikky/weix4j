package hk.guosen.weix.client.msg.usersvc;

import hk.guosen.weix.shared.msg.Music;

/**
 * 发送给客户的音乐信息
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 *
 */
public class UserMsgMusic extends UserMsg
{
	public Music music;

	public UserMsgMusic(String touser, Music music)
	{
		super(touser, "music");
		this.music = music;
	}
}
