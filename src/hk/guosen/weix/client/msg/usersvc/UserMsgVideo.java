package hk.guosen.weix.client.msg.usersvc;

import hk.guosen.weix.shared.msg.MediaEx;

/**
 * 发送给客户的视频消息
 * 
 * @author rikky.cai
 * @qq:6687523
 * @Email:6687523@qq.com
 *
 */
public class UserMsgVideo extends UserMsg
{
	
	public MediaEx video;

	public UserMsgVideo(String touser, MediaEx video)
	{
		super(touser, "video");
		this.video = video;
	}

}
